import numpy as np
from multiprocessing import Pool
import requests
from bs4 import BeautifulSoup
import pymongo
import json
import argparse
import dateparser
import re

from pymongo import MongoClient

def LOG(*args):
    print(*args)

client = MongoClient()
db = client["habr"]
posts_collection = db["posts"]
users_collection = db["users"]

def insert_post_to_db(post):
    if post == None:
        return None
    LOG("[load] {}".format(post["post_id"]))
    return posts_collection.insert_one(post).inserted_id

def insert_user_to_db(user):
    if user == None:
        return None
    LOG("[load] {}".format(user["nickname"]))
    return users_collection.insert_one(user).inserted_id

def normalize_number(num):
    if num.find("k") != -1:
        return int(float(num.rstrip("k").replace(",", ".")) * 1000)
    return int(num)


def get_post_by_id(pid):
    """
    Extract and transform habr post with comments

    :param int pid: Post id
                    Post should be available at https://habc.com/post/<pid>/
                    Else None will ge returned.
    :return: Dict with post and comments
    """
    
    try:
        addr = 'https://habr.com/post/' + str(pid) + '/'
        r = requests.get(addr, timeout=2)
        LOG("[extract] {} - done".format(addr))
    except Exception as e:
        LOG("[extract] Exception occured on {}".format(pid))
        return None

    soup = BeautifulSoup(r.text, 'html5lib')
    post = {}
    post['post_id'] = int(pid)
    post['url'] = addr
    if not soup.find("span", {"class": "post__title-text"}):
        post['status'] = 'title_not_found'
    else:
        post['status'] = 'ok'
        post['title'] = str(soup.find("span", {"class": "post__title-text"}).text)
        post['text'] = str(soup.find("div", {"class": "post__text"}).text)
        post['time'] = str(dateparser.parse(str(soup.find("span", {"class": "post__time"}).text), languages=["ru"]))
        post['author'] = str(soup.find("span", {"class": "user-info__nickname user-info__nickname_small"}).text)
        post['hubs'] = list(map(lambda x: x.text, soup.findAll("a", {"class": "hub-link"})))
        post['tags'] = list(map(lambda x: x.text, soup.findAll("a", {"class": "post__tag"})))
        post['score'] = int(str(soup.find("span", {"class": "js-score"}).text).lstrip("+").replace("–", "-"))
        post['favs_count'] = int(soup.find("span", {"class": "js-favs_count"}).text)
        post['views_count'] = normalize_number(soup.find("span", {"class": "post-stats__views-count"}).text)
        
        post['comments'] = []
        comments = soup.findAll("div", {"class": "comment"})
        def parse_comment(comment):
            if not comment.find("span", {"class": "user-info__nickname"}):
                return None
            comment_author = comment.find("span", {"class": "user-info__nickname"}).text
            comment_time = str(dateparser.parse(comment.find("time", {"class": "comment__date-time_published"}).text, languages=["ru"]))
            comment_id = comment['id']
            comment_text = comment.find("div", {"class": "comment__message"}).text
            comment_edited = len(comment.findAll("svg", {"class": "icon_comment-edit"})) > 0
            comment_parent = comment.find("a", {"class": "js-comment_parent"})
            if comment_parent is None:
                comment_parent = ""
            else:
                comment_parent = comment_parent['href'].lstrip("#")
            return {
                        "author": comment_author,
                        "time": comment_time,
                        "comment_id": comment_id,
                        "text": comment_text,
                        "edited": comment_edited,
                        "parent": comment_parent
                    }
        
        post['comments'] = list(filter(lambda x: not x is None, map(parse_comment, comments)))

    LOG("[transform] {}: {}".format(pid, post['status']))
    return post

def get_user_by_nickname(nickname):
    """ Extract and transform habr user """
    
    try:
        addr = 'https://habr.com/users/' + nickname + '/'
        r = requests.get(addr)
        LOG("[extract] {} - done".format(addr))
    except Exception as e:
        LOG("[extract] Exception occured on {}".format(nickname))
        return None

    soup = BeautifulSoup(r.text, 'html5lib')
    user = {}
    user['nickname'] = nickname
    user['url'] = addr
    if not soup.find("div", {"class": "stacked-counter__value"}):
        user['status'] = 'info_not_found'
    else:
        user['status'] = 'ok'
        user['fullname'] = getattr(soup.find("a", {"class": "user-info__fullname"}), 'text', "")
        user['specialization'] = soup.find("div", {"class": "user-info__specialization"}).text
        
        score = soup.find(text="Карма").findParent("a").find("div").text
        user['score'] = float(str(score).lstrip("+").replace("–", "-").replace(",", "."))
        rating = soup.find(text="Рейтинг").findParent("a").find("div").text
        user['rating'] = float(rating.replace("–", "-").replace(",", "."))
        subscribers = soup.find(text="Подписчики").findParent("a").find("div").text
        user['subscribers'] = float(subscribers.replace("–", "-").replace(",", "."))

        badges = soup.findAll("span", {"class": "profile-section__user-badge"})
        user['badges'] = list(map(lambda x: x.text, badges))

        user['about'] = getattr(soup.find("div", {"class": "profile-section__about-text"}), 'text', "")

        hubs = soup.findAll("a", {"class": "profile-section__user-hub"})
        user['hubs'] = list(map(lambda x: x.text, hubs))

        invited = soup.findAll("a", {"class": "list-snippet__nickname_small"})
        user['invited'] = list(map(lambda x: x.text, invited))

        invited = soup.find("p", {"class": "profile-section__invited"})
        if not invited:
            user['invited_by'] = ""
            user['invite_date'] = ""
        else:
            user['invited_by'] = invited.find("a").text
            user['invite_date'] = str(dateparser.parse(soup.find("p", {"class": "profile-section__invited"}).text.
                                                       split("в")[0].replace("\n", "").strip(), languages=["ru"]))

        links_ul = soup.find("ul", {"class": "profile-page__links"})
        if not links_ul:
            user['github'] = ""
            user['vk'] = ""
            user['facebook'] = ""
            user['twitter'] = ""
            user['instagram'] = ""
            user['livejournal'] = ""
        else:
            links = links_ul.findAll("a", {"class": "url"})
            hrefs = list(map(lambda x: x["href"], links))

            def extract_from_link(hrefs, substr):
                for h in hrefs:
                    if h.find(substr) != -1:
                        return h
                return ""

            user['github'] = extract_from_link(hrefs, "https://github.com")
            user['vk'] = extract_from_link(hrefs, "https://vk.com")
            user['facebook'] = extract_from_link(hrefs, "https://facebook.com")
            user['twitter'] = extract_from_link(hrefs, "https://twitter.com")
            user['instagram'] = extract_from_link(hrefs, "https://instagram.com")
            user['livejournal'] = extract_from_link(hrefs, ".livejournal.com")

        links = soup.find("div", {"class": "default-block__content_profile-summary"}).findAll("a", {"class": "defination-list__link"})

        def extract_text_from_link(links, substr):
            for link in links:
                if link['href'].find(substr) != -1:
                    return link.text
            return ""

        user['city'] = extract_text_from_link(links, "city")
        user['region'] = extract_text_from_link(links, "region")
        user['country'] = extract_text_from_link(links, "country")

        birthday = soup.find(text="Дата рождения")
        if not birthday:
            user['birthday'] = ""
        else:
            user['birthday'] = str(dateparser.parse(birthday.findNext("span").text, languages=["ru"]))

        registration_date = soup.find(text="Зарегистрирован")
        if not registration_date:
            user['registration_date'] = ""
        else:
            user['registration_date'] = str(dateparser.parse(registration_date.findNext("span").text, languages=["ru"]))
        user['in_rating'] = soup.find(text="В рейтинге").findNext("span").text.split("–")[0].replace("\n", "").strip()

        posts_number = soup.find("span", {"title": re.compile(r"\s*Публикации: \d*")})
        if not posts_number:
            user['posts_number'] = 0
        else:
            user['posts_number'] = normalize_number(posts_number.text)

        comments_number = soup.find("span", {"title": re.compile(r"\s*Комментарии: \d*")})
        if not comments_number:
            user['comments_number'] = 0
        else:
            user['comments_number'] = normalize_number(comments_number.text)

        company = soup.find(text="Работает в")
        if not company:
            user['company'] = ""
        else:
            user['company'] = str(company.findNext("span").find("a").text)
        

    LOG("[transform] {}: {}".format(nickname, user['status']))
    return user

def parse_posts(first, last, processes):
    with Pool(processes) as p:
        posts = list(p.map(get_post_by_id, np.arange(first, last)))

    LOG("Load")
    inserted_ids = list(map(insert_post_to_db, posts))
    return posts

def parse_users(nicknames, processes):
    with Pool(processes) as p:
        users = list(p.map(get_user_by_nickname, nicknames))

    LOG("Load")
    inserted_ids = list(map(insert_user_to_db, users))
    return users

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='ETL: habr posts')
    parser.add_argument('first', help='First post id to start from')
    parser.add_argument('last', help='Last post id')
    parser.add_argument('--processes', nargs='?', default=32, help='Number of processes')
    args = parser.parse_args()

    posts = parse_posts(int(args.first), int(args.last), int(args.processes))
    posts_authors = list(map(lambda x: x['author'], filter(lambda y: y != None and y['status'] == "ok", posts)))

    def users_from_comments(comments):
        return list(map(lambda x: x['author'], comments))

    posts_commentators = list(sum(map(lambda x: users_from_comments(x['comments']), filter(lambda y: y != None and y['status'] == "ok", posts)), []))
    nicknames = list(set(posts_authors + posts_commentators))
    parse_users(nicknames, int(args.processes))

    LOG("Done")