from pyspark.sql import SparkSession
from pyspark.sql.functions import *

sparkSession = SparkSession \
    .builder \
    .appName("habr") \
    .getOrCreate()

df = sparkSession.read \
    .format("com.mongodb.spark.sql.DefaultSource") \
    .option("spark.mongodb.input.uri", "mongodb://127.0.0.1/habr.users") \
    .load()

result = df \
    .filter(col("status") == 'ok') \
    .select("company", "score") \
    .groupBy("company") \
    .sum() \
    .select("company", col("sum(score)").alias("score"))

# result.sort(desc("score")).show()
result = result.sort('score', ascending=False)
result.write \
    .format("com.mongodb.spark.sql.DefaultSource") \
    .mode("overwrite") \
    .option("spark.mongodb.output.uri", "mongodb://127.0.0.1/habr.users") \
    .option("collection", "company_karma") \
    .save()
