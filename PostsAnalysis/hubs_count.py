from pyspark.sql import SparkSession
from pyspark.sql.functions import *

spark = SparkSession \
    .builder \
    .appName("habr") \
    .getOrCreate()
df = spark.read.format("com.mongodb.spark.sql.DefaultSource") \
    .option("uri", "mongodb://127.0.0.1/habr.posts") \
    .load()


def convert(row):
    words = row.split()
    if len(words) < 4:
        return None
    else:
        return words[1] + " " + words[2]


date_convert = udf(convert)
df = df.filter(df["status"] == "ok") \
    .withColumn("time", date_convert(col("time"))) \
    .select(explode("hubs").alias("hub"), "time") \
    .groupBy("hub", "time").count() \
    .filter(col("time").isNotNull())

df.filter(col("hub") == "Программирование").sort(desc("count")).show()
df.write \
    .format("com.mongodb.spark.sql.DefaultSource") \
    .mode("overwrite") \
    .option("spark.mongodb.output.uri", "mongodb://127.0.0.1/habr.posts") \
    .option("collection", "hub_date") \
    .save()
