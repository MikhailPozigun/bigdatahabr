# -*- coding: utf-8 -*-
from pyspark.sql import SparkSession
from pyspark.sql.functions import *
import string
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem.snowball import SnowballStemmer
import re
from pyspark.mllib.fpm import FPGrowth
from pyspark.context import SparkContext

def tokenize_ru(file_text):
	# firstly let's apply nltk tokenization
	tokens = word_tokenize(file_text)

	# let's delete punctuation symbols
	tokens = [i for i in tokens if (i not in string.punctuation)]

	# deleting stop_words
	stop_words = stopwords.words('russian')
	stop_words.extend(['что', 'это', 'так', 'вот', 'быть', 'как', 'в', '—', '–', 'к', 'на', '...'])
	tokens = [i for i in tokens if (i not in stop_words)]

	# cleaning words
	# tokens = [i.replace("«", "").replace("»", "") for i in tokens]

	return tokens


sparkSession = SparkSession \
    .builder \
    .appName("habr").getOrCreate()
df = sparkSession.read \
	.format("com.mongodb.spark.sql.DefaultSource") \
	.option("spark.mongodb.input.uri","mongodb://127.0.0.1/habr.posts") \
	.load()

df = df \
	.filter(df['status'] == 'ok') \
	.select('title')

df_pd = df.toPandas()

stemmer = SnowballStemmer('russian')
reg = re.compile(r'[\d+\\/+*%#<>&\^~\-|()``$@=\'«»№—]')
words = []
for elem in df_pd['title']:
	title = elem.lower()
	title = re.sub(reg, "", title) 
	tokens = tokenize_ru(title)
	stemmed_words = ([stemmer.stem(w) for w in tokens])
	if len(stemmed_words) != 0:
		words.append(list(set(stemmed_words)))

words_map = {}
for words_of_title in words:
	for word in words_of_title:
		words_map[word] = []

# print("TOKENS", len(tokens))
sc = SparkContext.getOrCreate()
rdd = sc.parallelize(words, 2)
# words = words.map(lambda row: row[0])
model = FPGrowth.train(rdd, 0.001, 2)
result = model.freqItemsets().collect()

for word in words_map:
	for i in range(len(result)):
		if word in result[i][0]:
			words_map[word] = words_map[word] + result[i][0]

for word in words_map:
	words_map[word] = list(set(words_map[word]))
	if word in words_map[word]:
		words_map[word].remove(word)

f = open("./BigData/bigdatahabr/PostsAnalysis/out_titles_words.txt", "w")
empty_assoc_count = 0
for k in words_map:
	if len(words_map[k]) == 0:
		empty_assoc_count = empty_assoc_count + 1
	print(k, words_map[k], file=f)
print("No associations %: ", empty_assoc_count, " ", len(words_map), " ", empty_assoc_count / len(words_map) * 100.0, "%", file=f)
f.close()

# f = open("./BigData/bigdatahabr/PostsAnalysis/out_titles.txt", "w")
# print(result, file=f)
# f.close()

# df_filtered.write \
# 	.format("com.mongodb.spark.sql.DefaultSource") \
# 	.mode("overwrite") \
# 	.option("spark.mongodb.output.uri","mongodb://127.0.0.1/habr.posts") \
# 	.option("collection", "posts_filtered") \
# 	.save()
