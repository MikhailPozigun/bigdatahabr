# -*- coding: utf-8 -*-
from pyspark.sql import SparkSession
from pyspark.sql.functions import *
import string
import copy
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem.snowball import SnowballStemmer
import re
from pyspark.mllib.fpm import FPGrowth
from pyspark.context import SparkContext

sparkSession = SparkSession \
    .builder \
    .appName("habr").getOrCreate()
df = sparkSession.read \
	.format("com.mongodb.spark.sql.DefaultSource") \
	.option("spark.mongodb.input.uri","mongodb://127.0.0.1/habr.posts") \
	.load()

df = df \
	.filter(df['status'] == 'ok') \
	.select('hubs')

df_pandas = df.toPandas()
# print("PANDAS", df_pandas['tags'])
hubs_lists_by_posts = []

for hubs_of_post in df_pandas['hubs']:
	hubs_lists_by_posts.append(hubs_of_post)

# hubs_lists_by_posts_copy = copy.deepcopy(hubs_lists_by_posts)
# unique_hubs = []
# for i in range(len(tags_lists_by_posts_copy)):
# 	for tag in tags_lists_by_posts_copy[i]:
# 		isUnique = True
# 		for j in range(len(tags_lists_by_posts_copy)):
# 			if j == i:
# 				continue
# 			if tag in tags_lists_by_posts_copy[j]:
# 				isUnique = False
# 				break
# 		if isUnique is True:
# 			tags_lists_by_posts[i].remove(tag)
# 			unique_tags.append(tag)

# f = open("./BigData/bigdatahabr/PostsAnalysis/unique_tags.txt", "w")
# for tag in unique_tags:
# 	print(tag, file=f)
# print(len(unique_tags), file=f)
# f.close()

hubs_map = {}
for hubs_of_post in hubs_lists_by_posts:
	for hub in hubs_of_post:
		hubs_map[hub] = []

# f = open("./BigData/bigdatahabr/PostsAnalysis/out.txt", "w")
# print(tags_list, file=f)
# f.close()

sc = SparkContext.getOrCreate()
rdd = sc.parallelize(hubs_lists_by_posts, 2)
# words = words.map(lambda row: row[0])
model = FPGrowth.train(rdd, 0.002, 2)
result = model.freqItemsets().collect()

for hub in hubs_map:
	for i in range(len(result)):
		if hub in result[i][0]:
			hubs_map[hub] = hubs_map[hub] + result[i][0]

for hub in hubs_map:
	hubs_map[hub] = list(set(hubs_map[hub]))
	if hub in hubs_map[hub]:
		hubs_map[hub].remove(hub)

f = open("./BigData/bigdatahabr/PostsAnalysis/out_hubs.txt", "w")
empty_assoc_count = 0
for k in hubs_map:
	if len(hubs_map[k]) == 0:
		empty_assoc_count = empty_assoc_count + 1
	print(k, hubs_map[k], file=f)
print("No associations %: ", empty_assoc_count, " ", len(hubs_map), " ", empty_assoc_count / len(hubs_map) * 100.0, "%", file=f)
f.close()

df_hubs_assoc = sparkSession.createDataFrame(zip(list(hubs_map.keys()), list(hubs_map.values())), schema=['hub', 'associated_hubs'])

df_hubs_assoc.write \
	.format("com.mongodb.spark.sql.DefaultSource") \
	.mode("overwrite") \
	.option("spark.mongodb.output.uri","mongodb://127.0.0.1/habr.posts") \
	.option("collection", "hubs_assoc") \
	.save()

