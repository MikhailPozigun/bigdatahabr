from pyspark.sql import SparkSession
from pyspark.sql.functions import *
import pandas as pd
import numpy as np
# import pyspark
sparkSession = SparkSession \
    .builder \
    .appName("habr").getOrCreate()
df = sparkSession.read \
	.format("com.mongodb.spark.sql.DefaultSource") \
	.option("spark.mongodb.input.uri","mongodb://127.0.0.1/habr.posts") \
	.load()

df_filtered = df \
	.filter(df['status'] == 'ok') \
	.sort('score', ascending=False) \
	.limit(3)
df_filtered.select('title').show(3, False)
df_pandas = df_filtered.toPandas()
# print("PANDAS", df_pandas['tags'])
listed_tags = []
for e in df_pandas['tags']:
	listed_tags = listed_tags + e
listed_hubs = []
for e in df_pandas['hubs']:
	listed_hubs = listed_hubs + e

file = open("./BigData/posts_out.txt", "w")
for title in df_pandas['title']:
	print(title, file=file)
print("TAGS: ", set(listed_tags), file=file)
print("HUBS: ", set(listed_hubs), file=file)
file.close()
# tags_df = df_filtered.withColumn("total", concat_ws(",", df_filtered.tags)).select("total")

# tags_df.write \
# 	.format("com.mongodb.spark.sql.DefaultSource") \
# 	.mode("overwrite") \
# 	.option("spark.mongodb.output.uri","mongodb://127.0.0.1/habr.posts") \
# 	.option("collection", "top_tags_") \
# 	.save()