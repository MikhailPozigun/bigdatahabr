from pyspark.sql import SparkSession
from pyspark.ml.regression import RandomForestRegressor
from pyspark.ml.tuning import CrossValidator, ParamGridBuilder
from pyspark.ml.evaluation import RegressionEvaluator
from pyspark.ml.regression import LinearRegression
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.feature import StandardScaler
from pyspark.ml import Pipeline
from pyspark.sql.types import StructType, StructField, DoubleType, StringType
from pyspark.sql.functions import *
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
# import pyspark
sparkSession = SparkSession \
    .builder \
    .appName("habr").getOrCreate()
df_ranking_rf = sparkSession.read \
	.format("com.mongodb.spark.sql.DefaultSource") \
	.option("spark.mongodb.input.uri","mongodb://127.0.0.1/habr.feature_rank_rf") \
	.load()

df_ranking_rf = df_ranking_rf.sort('values', ascending=True)

pdf_ranking_rf = df_ranking_rf.toPandas()
ax = pdf_ranking_rf.plot.bar(title='Posts Features Ranking Via RF', alpha=0.7)
plt.xticks(range(len(pdf_ranking_rf['features'])),(pdf_ranking_rf['features'][0], \
    pdf_ranking_rf['features'][1], pdf_ranking_rf['features'][2]), rotation=0)
for patch in ax.patches:
    bl = patch.get_xy()
    x = 0.5 * patch.get_width() + bl[0]
    y = 0.8 * patch.get_height() + bl[1] 
    ax.text(x,y,"%.3f" %(patch.get_height()),
            ha='center', rotation='horizontal', weight = 'bold', color='white')

# plt.figure()

# df_ranking_regr = sparkSession.read \
#     .format("com.mongodb.spark.sql.DefaultSource") \
#     .option("spark.mongodb.input.uri","mongodb://127.0.0.1/habr.feature_rank_regr") \
#     .load()

# pdf_ranking_regr  = df_ranking_regr.toPandas()
# pdf_ranking_regr['values'] = pdf_ranking_regr['values'].abs()
# ax = pdf_ranking_regr.plot.bar(title='Features Ranking Via Linear Regression')
# plt.xticks(range(len(pdf_ranking_regr['features'])),(pdf_ranking_regr['features'][0], \
#     pdf_ranking_regr['features'][1], pdf_ranking_regr['features'][2]), rotation=0)
# for patch in ax.patches:
#     bl = patch.get_xy()
#     x = 0.5 * patch.get_width() + bl[0]
#     y = 0.6 * patch.get_height() + bl[1] 
#     ax.text(x,y,"%.2f" %(patch.get_height()),
#             ha='center', rotation='horizontal', weight = 'bold', color='white')

plt.show()
