from pyspark.sql import SparkSession
from pyspark.ml.regression import RandomForestRegressor
from pyspark.ml.tuning import CrossValidator, ParamGridBuilder
from pyspark.ml.evaluation import RegressionEvaluator
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.feature import StandardScaler
from pyspark.ml import Pipeline
from pyspark.sql.types import StructType, StructField, DoubleType, StringType
from pyspark.sql.functions import *
import pandas as pd
# import pyspark
sparkSession = SparkSession \
    .builder \
    .appName("habr").getOrCreate()
df = sparkSession.read \
	.format("com.mongodb.spark.sql.DefaultSource") \
	.option("spark.mongodb.input.uri","mongodb://127.0.0.1/habr.posts") \
	.load()

df = df \
	.filter(df['status'] == 'ok') \
	.select('*', size('comments').alias('comments_count')) \
	.select('post_id', 'favs_count', 'views_count', 'comments_count', 'score')

features = ["favs_count", "comments_count", "views_count"]  
lr_data = df.select(col("score").alias("label"), *features)  
# lr_data.printSchema()

vector = VectorAssembler(inputCols=features, outputCol="features")
scaler = StandardScaler(inputCol="features", outputCol="scaled_features")
rfr = RandomForestRegressor(labelCol="label", featuresCol="scaled_features")

stages = [vector, scaler, rfr]

pipe = Pipeline(stages=stages)

estimatorParam = ParamGridBuilder() \
	.addGrid(rfr.maxDepth, [4, 6, 8]) \
	.addGrid(rfr.maxBins, [5, 10, 20, 40]) \
	.addGrid(rfr.impurity, ["variance"]) \
	.build()

evaluator = RegressionEvaluator(labelCol="label", predictionCol="prediction", metricName="r2")

crossval = CrossValidator(estimator=pipe,
                         estimatorParamMaps=estimatorParam,
                         evaluator=evaluator,
                         numFolds=3)

cvmodel = crossval.fit(lr_data)

model = pd.DataFrame(cvmodel.bestModel.stages[-1].featureImportances.toArray(), columns=["values"])
features_col = pd.Series(features)
model["features"] = features_col

new_schema = StructType([
  StructField("values", DoubleType(), False),
  StructField("features", StringType(), False)
])

feature_importance = sparkSession.createDataFrame(model, schema=new_schema)

# display(feature_importance.orderBy("values", ascending=False))
feature_importance.write \
	.format("com.mongodb.spark.sql.DefaultSource") \
	.mode("overwrite") \
	.option("spark.mongodb.output.uri","mongodb://127.0.0.1/habr.posts") \
	.option("collection", "feature_rank_rf") \
	.save()
