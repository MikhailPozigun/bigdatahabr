from pyspark.sql import SparkSession
from pyspark.ml.regression import LinearRegression
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.feature import StandardScaler
from pyspark.ml import Pipeline
from pyspark.sql.types import StructType, StructField, DoubleType, StringType
from pyspark.sql.functions import *
import pandas as pd
import numpy as np
# import pyspark
sparkSession = SparkSession \
    .builder \
    .appName("habr").getOrCreate()
df = sparkSession.read \
	.format("com.mongodb.spark.sql.DefaultSource") \
	.option("spark.mongodb.input.uri","mongodb://127.0.0.1/habr.posts") \
	.load()

df = df \
	.filter(df['status'] == 'ok') \
	.select('*', size('comments').alias('comments_count')) \
	.select('post_id', 'favs_count', 'views_count', 'comments_count', 'score')

features = ["favs_count", "comments_count", "views_count"]  
lr_data = df.select(col("score").alias("label"), *features)  
# lr_data.printSchema()

(training, test) = lr_data.randomSplit([.9, .1])
vectorAssembler = VectorAssembler(inputCols=features, outputCol="unscaled_features")
standardScaler = StandardScaler(inputCol="unscaled_features", outputCol="features")
lr = LinearRegression(maxIter=100, regParam=.01)

stages = [vectorAssembler, standardScaler, lr]
pipeline = Pipeline(stages=stages)
model = pipeline.fit(training)
# prediction = model.transform(test)
# pred = prediction.select("favs_count", "comments_count", "views_count", "features", "prediction")
# pred.show(20, False)
# print("COEFFICIENTS ", model.stages[-1].coefficients[0])
coeffs = [model.stages[-1].coefficients[0].item(), model.stages[-1].coefficients[1].item(), model.stages[-1].coefficients[2].item()]
feature_importance = sparkSession.createDataFrame(zip(coeffs, features), schema=['values', 'features'])
# feature_importance.show()
feature_importance.write \
	.format("com.mongodb.spark.sql.DefaultSource") \
	.mode("overwrite") \
	.option("spark.mongodb.output.uri","mongodb://127.0.0.1/habr.posts") \
	.option("collection", "feature_rank_regr") \
	.save()
